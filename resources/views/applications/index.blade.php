@extends('layouts.app')

@section('content')

<h1>ciao {{$applications[0]['first_name']}}</h1>
        
        <div>
            <table>
                @foreach($applications as $application)
                <tr>
                    <td>{{ $application['first_name'] }}</td>
                    <td>{{ $application['last_name'] }}</td>
                    <td><a href="" onclick="approve({{$application['id']}})">{{__("Approve")}}</a></td>
                    <td><a href="" onclick="">{{__("Refuse")}}</a></td>
                </tr>
                @endforeach
            </table>


        </div>

<script>
    function approve(applicationId){
        console.log(applicationId);
        fetch('approve', {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json, text-plain, */*",
                    },
                method: 'post',
                credentials: "same-origin",
                body: JSON.stringify({
                    applicationId: 'applicationId',
                })
            })
            .then((data) => {
                window.location.href = 'applications.index';
            })
            .catch(function(error) {
                console.log(error);
        });
    }
</script>
@endsection