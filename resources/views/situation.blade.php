@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <div class="container">
            <h1>Candidatura</h1>
        </div>
    </section>
    <section>
        {{__("Your application is ") }} {{$application->status}}
    </section>
@stop
