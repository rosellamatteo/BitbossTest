<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Application;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    public function homepage() {
        return view('home');
    }

    public function apply() {
        return view('apply');
    }

    public function postApply(Request $request) {
        $id = Auth::id();
        $request->user_id = $id;
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:applications',
            'phone' => 'required'
        ]);


        $application = Application::create([
            'user_id' => $id,
            'first_name' => $request['first_name'], 
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'notes' => $request['notes'],
            'status' => 'pending'
        ]);

        return view('situation')->with('application',$application);
    }

    public function situation(Request $request){
        $id = Auth::id();
        $application = Application::where('user_id' , '=' , $id)->first();
        return view('situation')->with('application' , $application);
    }
}
