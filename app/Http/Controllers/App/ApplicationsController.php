<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Http\Request;

class ApplicationsController extends Controller
{

    public function __construct()
    {
        // Accessible only to admins
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $applications = Application::where('status' , '=' , 'pending')->paginate(20);
        return view('applications.index', ['applications'=>$applications]);
    }



    /**
     * @param Application $application
     */
    public function accept(Application $applicationId)
    {
        Application::where('id', $applicationId)
                ->update(['status' => 'accepted']);
        return;
    }

    public function refuse(Application $applicationId)
    {
        Application::where('id', $applicationId)
                ->update(['status' => 'refuse']);
        return;
    }

}
